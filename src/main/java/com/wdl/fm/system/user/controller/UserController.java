package com.wdl.fm.system.user.controller;

import com.wdl.fm.common.controller.BaseController;
import com.wdl.fm.common.entity.DataTableResult;
import com.wdl.fm.common.entity.Result;
import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.SysUser;
import com.wdl.fm.common.utils.DatatableUtils;
import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/6/13.
 */
@Controller
@RequestMapping("/admin/user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String showUserListView() {
        return "admin/user/user";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String showUserAddView() {
        return "admin/user/user_add";
    }

    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public DataTableResult getUserList(HttpServletRequest request) {
        String keyword = request.getParameter("keyword");
        Page<SysUser> pageBean = getPageBean(request);

        DataTableResult<SysUser> dataTableResult = new DataTableResult<>();
        return DatatableUtils.getDataTableResult(dataTableResult, userService.getPageList(pageBean, keyword));
    }

    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Result addUser(SysUser user) {
        return getCommonDataResult(userService.addUser(user));
    }

    @ResponseBody
    @RequestMapping(value = "rpwd", method = RequestMethod.POST)
    public Result addUser(HttpServletRequest request) {
        String pwd = request.getParameter("pwd");
        String id = request.getParameter("id");
        return getCommonDataResult(userService.resetPwd(id, pwd));
    }

    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    public Result delRole(@RequestParam("list[]") List<String> ids) {
        Result result = new Result();
        result.setMessage(userService.delUsers(ids));
        result.setSuccess(true);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "stree", method = RequestMethod.POST)
    public List<TreeNodeSimple> getSimpleTree(HttpServletRequest request) {
        String userID = request.getParameter("id");

        return userService.getAllRoleTreeUserChecked(userID);
    }

    @ResponseBody
    @RequestMapping(value = "changepermission", method = RequestMethod.POST)
    public Result changeRolePermission(HttpServletRequest request) {
        String userID = request.getParameter("uid");
        String roleID = request.getParameter("rid");
        boolean checked = Boolean.parseBoolean(request.getParameter("checked"));
        boolean isGroup = Boolean.parseBoolean(request.getParameter("isGroup"));

        if (isGroup)
            userService.changeUserRoleGroup(userID, roleID, checked);
        else
            userService.changeUserRole(userID, roleID, checked);

        return getCommonDataResult(true);
    }
}
