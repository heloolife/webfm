package com.wdl.fm.system.user.service;

import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.exception.LoginException;
import com.wdl.fm.common.model.SysPermission;
import com.wdl.fm.common.model.SysRole;
import com.wdl.fm.common.model.SysUser;
import com.wdl.fm.common.model.SysUserRole;
import com.wdl.fm.core.orm.mybatis.Page;

import java.util.List;

/**
 * Created by Administrator on 2017/6/6.
 */
public interface UserService {
    /**
     * 获取用户的角色列表
     *
     * @param id
     * @return
     */
    List<SysRole> getRolesByUserID(String id);

    /**
     * 为了兼容admin 获取全部权限
     *
     * @param id
     * @param name
     * @return
     */
    List<SysRole> getRolesByUserID(String id, String name);

    /**
     * 通过用户名获取权限列表
     *
     * @param id
     * @return
     */
    List<SysPermission> getPermissionsByUserID(String id);

    /**
     * 为了兼容admin
     *
     * @param id
     * @param name
     * @return
     */
    List<SysPermission> getPermissionsByUserID(String id, String name);

    /**
     * 通过用户名获取用户
     *
     * @param name
     * @return
     */
    SysUser getUserByName(String name);

    /**
     * 通过用户id获取用户和角色的对应关系
     *
     * @param userID
     * @return
     */
    List<SysUserRole> getUserRoles(String userID);

    /**
     * 用户简单认证
     *
     * @param name
     * @param pwd
     */
    void authenticationUser(String name, String pwd) throws LoginException;

    /**
     * 获取用户的页列表
     *
     * @param page
     * @return
     */
    Page<SysUser> getPageList(Page<SysUser> page, String keyword);

    /**
     * 通过id获取用户
     *
     * @param id
     * @return
     */
    SysUser getUserByID(String id);

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    boolean addUser(SysUser user);

    /**
     * 更新用户信息
     *
     * @param user
     * @return
     */
    boolean updateUser(SysUser user);

    /**
     * 删除用户
     * @param id
     * @return
     */
    boolean delUser(String id);

    /**
     * 删除用户
     * @param ids
     * @return
     */
    String delUsers(List<String> ids);

    /**
     * 重置密码
     * @param id
     * @param pwd
     * @return
     */
    boolean resetPwd(String id,String pwd);

    /**
     * 获取所有角色  其中用户对应的角色选择上
     * @param userID
     * @return
     */
    List<TreeNodeSimple> getAllRoleTreeUserChecked(String userID);

    /**
     * 用户是否存在角色
     * @param userID
     * @param roleID
     * @return
     */
    boolean hasRole(String userID,String roleID);

    /**
     * 用户添加角色
     * @param userID
     * @param roleID
     * @return
     */
    boolean addUserRole(String userID,String roleID);

    /**
     * 用户删除角色
     * @param userID
     * @param roleID
     * @return
     */
    boolean delUserRole(String userID,String roleID);

    boolean changeUserRole(String userID,String roleID,boolean checked);

    boolean changeUserRoleGroup(String userID,String roleID,boolean checked);
}
