package com.wdl.fm.system.authority.service.impl;

import com.wdl.fm.common.dao.SysPermissionMapper;
import com.wdl.fm.common.entity.tree.TreeNode;
import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.SysPermission;
import com.wdl.fm.common.model.SysPermissionExample;
import com.wdl.fm.common.utils.ApplicationUtils;
import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.authority.bo.Permission;
import com.wdl.fm.system.authority.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/6.
 */
@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Override
    public List<SysPermission> getAllPermissions() {
        return sysPermissionMapper.selectByExample(new SysPermissionExample());
    }

    @Override
    public SysPermission getPermissionByID(String id) {
        return sysPermissionMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SysPermission> getPageList(Page<SysPermission> page, String keyword, String groupid) {
        SysPermissionExample example = new SysPermissionExample();
        SysPermissionExample.Criteria criteria = example.createCriteria();
        if (keyword != null && !"".equals(keyword))
            criteria.andNameLike("%" + keyword + "%");
        if (groupid != null && !"".equals(groupid))
            criteria.andPidEqualTo(groupid);

        example.setOrderByClause("create_time asc");

        List<SysPermission> list = sysPermissionMapper.selectByExample(page, example);
        page.setResult(list);
        return page;
    }

    @Override
    public Page<Permission> getPageListModel(Page<Permission> page, String keyword, String groupid) {

        SysPermissionExample example = new SysPermissionExample();
        SysPermissionExample.Criteria criteria = example.createCriteria();
        if (keyword != null && !"".equals(keyword))
            criteria.andNameLike("%" + keyword + "%");
        if (groupid != null && !"".equals(groupid))
            criteria.andPidEqualTo(groupid);

        example.setOrderByClause("create_time asc");

        List<Permission> permissions = sysPermissionMapper.selectByExampleModel(page, example);
        for (Permission info : permissions) {
            if (info.getPid() != null && !"".equals(info.getPid()))
                info.setGroupName(getPermissionByID(info.getPid()).getName());
        }

        page.setResult(permissions);

        return page;
    }

    @Override
    public List<TreeNode> getTreeNodesByID(String id) {
        List<TreeNode> treeNodes = new ArrayList<>();

        SysPermissionExample example = new SysPermissionExample();
        if (id == null)
            id = "";
        example.createCriteria().andPidEqualTo(id);
        List<SysPermission> list = sysPermissionMapper.selectByExample(example);

        TreeNode node;
        for (SysPermission info : list) {
            node = new TreeNode();
            node.setId(info.getId());
            node.setName(info.getName());
            node.setIsParent(hasChilds(info.getId()));

            treeNodes.add(node);
        }

        return treeNodes;
    }

    @Override
    public boolean hasChilds(String id) {
        SysPermissionExample example = new SysPermissionExample();
        example.createCriteria().andPidEqualTo(id);
        return sysPermissionMapper.selectByExample(example).size() > 0;
    }

    @Override
    public boolean addPermission(SysPermission permission) {
        int insert = sysPermissionMapper.insert(permission);
        return insert > 0;
    }

    @Override
    public boolean updatePermission(SysPermission permission) {
        int i = sysPermissionMapper.updateByPrimaryKey(permission);
        return i > 0;
    }

    @Override
    public boolean editPermission(SysPermission permission) {
        if (permission.getId() == null || "".equals(permission.getId())) {
            permission.setId(ApplicationUtils.getUUID());
            return addPermission(permission);
        } else {
            return updatePermission(permission);
        }
    }

    @Override
    public boolean delPermission(String id) {
        return sysPermissionMapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public int delPermissionAndChild(String id) {
        SysPermissionExample example = new SysPermissionExample();
        example.createCriteria().andIdEqualTo(id);
        example.or().andPidEqualTo(id);
        return sysPermissionMapper.deleteByExample(example);
    }

    @Override
    public String delPermissions(List<String> ids) {
        int delCount = 0;
        for (String id : ids) {
            delCount += delPermissionAndChild(id);
        }
        return "成功删除" + delCount + "条数据";
    }

    @Override
    public List<TreeNodeSimple> getSimpleTreeNodes() {
        return getTree(sysPermissionMapper.selectByExample(new SysPermissionExample()));
    }

    @Override
    public List<SysPermission> getPermissionByPid(String pid) {
        SysPermissionExample example = new SysPermissionExample();
        example.createCriteria().andPidEqualTo(pid);
        return sysPermissionMapper.selectByExample(example);
    }

    @Override
    public List<SysPermission> getPermissionGroup() {
        SysPermissionExample example = new SysPermissionExample();
        example.createCriteria().andTypeEqualTo(2);
        return sysPermissionMapper.selectByExample(example);
    }

    @Override
    public List<TreeNodeSimple> getPermissionGroupTree() {
        SysPermissionExample example = new SysPermissionExample();
        example.createCriteria().andTypeEqualTo(2);

        return getTree(sysPermissionMapper.selectByExample(example));
    }

    private List<TreeNodeSimple> getTree(List<SysPermission> list) {
        List<TreeNodeSimple> tree = new ArrayList<>();

        TreeNodeSimple node;
        for (SysPermission info : list) {
            node = new TreeNodeSimple();
            node.setId(info.getId());
            node.setpId(info.getPid());
            node.setName(info.getName());
            if (info.getType() == 2)
                node.setIsParent(true);

            tree.add(node);
        }

        return tree;
    }
}
