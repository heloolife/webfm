package com.wdl.fm.system.authority.service.impl;

import com.wdl.fm.common.dao.SysRoleMapper;
import com.wdl.fm.common.dao.SysRolePermissionMapper;
import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.*;
import com.wdl.fm.common.utils.ApplicationUtils;
import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.authority.service.PermissionService;
import com.wdl.fm.system.authority.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/6.
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

    @Autowired
    private PermissionService permissionService;

    @Override
    public List<SysRole> getAllRoles() {
        return sysRoleMapper.selectByExample(new SysRoleExample());
    }

    @Override
    public List<SysRole> getListByPid(String pid) {
        return null;
    }

    @Override
    public List<SysRole> getRoles(List<SysUserRole> userRoles) {
        List<SysRole> list = new ArrayList<>();
        for (SysUserRole info : userRoles) {
            list.add(sysRoleMapper.selectByPrimaryKey(info.getRoleId()));
        }
        return list;
    }

    @Override
    public List<SysPermission> getPermissionsByRoleID(String roleID) {
        List<SysPermission> list = new ArrayList<>();

        List<SysRolePermission> rolePermissionsByRoleID = getRolePermissionsByRoleID(roleID);
        SysPermission permission;
        for (SysRolePermission info : rolePermissionsByRoleID) {
            permission = permissionService.getPermissionByID(info.getPermissionId());
            if (permission != null)
                list.add(permission);
        }

        return list;
    }

    @Override
    public List<SysRolePermission> getRolePermissionsByRoleID(String roleID) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        example.createCriteria().andRoleIdEqualTo(roleID);
        return sysRolePermissionMapper.selectByExample(example);
    }

    @Override
    public Page<SysRole> getPageList(Page<SysRole> page, String keyword) {
        SysRoleExample example = new SysRoleExample();
        if (keyword != null && !"".equals(keyword))
            example.createCriteria().andRolenameLike("%" + keyword + "%");
        List<SysRole> list = sysRoleMapper.selectByExample(page, example);
        page.setResult(list);
        return page;
    }

    @Override
    public SysRole getRoleByID(String id) {
        return sysRoleMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean delRoleByID(String id) {
        return sysRoleMapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public String delRoles(List<String> ids) {
        int count = 0;
        for (String id : ids) {
            if (delRoleByID(id))
                count++;
        }
        return "成功删除" + count + "条数据，失败" + (ids.size() - count) + "条";
    }

    @Override
    public boolean addRole(SysRole role) {
        return sysRoleMapper.insert(role) > 0;
    }

    @Override
    public boolean updateRole(SysRole role) {
        return sysRoleMapper.updateByPrimaryKeySelective(role) > 0;
    }

    @Override
    public boolean editRole(SysRole role) {
        if (role.getId() == null || "".equals(role.getId())) {
            role.setId(ApplicationUtils.getUUID());
            return addRole(role);
        }
        return updateRole(role);
    }

    @Override
    public List<TreeNodeSimple> getPermissionTree(String id) {
        List<TreeNodeSimple> simpleTreeNodes = permissionService.getSimpleTreeNodes();
        for (int i = 0; i < simpleTreeNodes.size(); i++) {
            if (isHasRolePermission(id, simpleTreeNodes.get(i).getId()))
                simpleTreeNodes.get(i).setChecked(true);
        }
        return simpleTreeNodes;
    }

    @Override
    public boolean isHasRolePermission(String roleID, String permissionID) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        example.createCriteria().andPermissionIdEqualTo(permissionID).andRoleIdEqualTo(roleID);
        return sysRolePermissionMapper.selectByExample(example).size() > 0;
    }

    @Override
    public boolean changeRolePermission(String roleID, String permissionID, boolean checked) {
        if (checked)
            return addRolePermisson(roleID, permissionID);

        return delRolePermission(roleID, permissionID);
    }

    @Override
    public boolean changeRolePermissionGroup(String roleID, String permissionGroupID, boolean checked) {
        List<SysPermission> permissionByPid = permissionService.getPermissionByPid(permissionGroupID);
        for (SysPermission info : permissionByPid) {
            if (checked) {
                if (!isHasRolePermission(roleID, info.getId()))
                    addRolePermisson(roleID, info.getId());
            } else {
                delRolePermission(roleID, info.getId());
            }
        }
        return true;
    }

    @Override
    public boolean addRolePermisson(String roleID, String permissionID) {
        SysRolePermission rolePermission = new SysRolePermission();
        rolePermission.setPermissionId(permissionID);
        rolePermission.setRoleId(roleID);
        return sysRolePermissionMapper.insert(rolePermission) > 0;
    }

    @Override
    public boolean delRolePermission(String roleID, String permissionID) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        example.createCriteria().andRoleIdEqualTo(roleID).andPermissionIdEqualTo(permissionID);

        return sysRolePermissionMapper.deleteByExample(example) > 0;
    }
}
