package com.wdl.fm.system.authority.service;

import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.SysPermission;
import com.wdl.fm.common.model.SysRole;
import com.wdl.fm.common.model.SysRolePermission;
import com.wdl.fm.common.model.SysUserRole;
import com.wdl.fm.core.orm.mybatis.Page;

import java.util.List;

/**
 * Created by Administrator on 2017/6/6.
 */
public interface RoleService {
    /**
     * 获取所有角色
     *
     * @return
     */
    List<SysRole> getAllRoles();

    List<SysRole> getListByPid(String pid);

    /**
     * 通过对应关系获取角色列表
     *
     * @param userRoles
     * @return
     */
    List<SysRole> getRoles(List<SysUserRole> userRoles);

    /**
     * 获取角色对应的权限列表
     *
     * @param roleID
     * @return
     */
    List<SysPermission> getPermissionsByRoleID(String roleID);

    /**
     * 获取角色权限对应关系
     *
     * @param roleID
     * @return
     */
    List<SysRolePermission> getRolePermissionsByRoleID(String roleID);

    /**
     * 获取分页数据
     *
     * @param page
     * @return
     */
    Page<SysRole> getPageList(Page<SysRole> page, String keyword);

    /**
     * 通过id获取
     *
     * @param id
     * @return
     */
    SysRole getRoleByID(String id);

    /**
     * 删除角色
     *
     * @param id
     * @return
     */
    boolean delRoleByID(String id);

    /**
     * 删除角色列表
     *
     * @param ids
     * @return
     */
    String delRoles(List<String> ids);

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    boolean addRole(SysRole role);

    /**
     * 更新角色
     *
     * @param role
     * @return
     */
    boolean updateRole(SysRole role);

    /**
     * 编辑角色 根据id是否为空来进行角色的添加或更新
     *
     * @param role
     * @return
     */
    boolean editRole(SysRole role);

    /**
     * 获取全部权限 role对应的权限也标注出来
     * @param id
     * @return
     */
    List<TreeNodeSimple> getPermissionTree(String id);

    /**
     * 该角色是否有该权限
     * @param roleID
     * @param permissionID
     * @return
     */
    boolean isHasRolePermission(String roleID,String permissionID);

    /**
     * 改变role对应的permission
     * @param roleID
     * @param permissionID
     * @return
     */
    boolean changeRolePermission(String roleID,String permissionID,boolean checked);

    /**
     * 改变role
     * @param roleID
     * @param permissionGroupID
     * @param checked
     * @return
     */
    boolean changeRolePermissionGroup(String roleID,String permissionGroupID,boolean checked);

    /**
     * 添加角色权限
     * @param roleID
     * @param permissionID
     * @return
     */
    boolean addRolePermisson(String roleID,String permissionID);

    /**
     * 删除角色对应权限
     * @param roleID
     * @param permissionID
     * @return
     */
    boolean delRolePermission(String roleID,String permissionID);

}
