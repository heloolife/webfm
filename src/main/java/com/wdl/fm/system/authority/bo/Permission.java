package com.wdl.fm.system.authority.bo;

import com.wdl.fm.common.model.SysPermission;

/**
 * Created by Administrator on 2017/6/12.
 */
public class Permission extends SysPermission {
    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
