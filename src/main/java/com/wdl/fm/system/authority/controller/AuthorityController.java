package com.wdl.fm.system.authority.controller;

import com.wdl.fm.common.controller.BaseController;
import com.wdl.fm.common.entity.DataTableResult;
import com.wdl.fm.common.entity.Result;
import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.SysPermission;
import com.wdl.fm.common.model.SysRole;
import com.wdl.fm.common.utils.DatatableUtils;
import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.authority.bo.Permission;
import com.wdl.fm.system.authority.service.PermissionService;
import com.wdl.fm.system.authority.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */
@Controller
@RequestMapping("/admin/auth")
public class AuthorityController extends BaseController {
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RoleService roleService;

    /**********************************权限相关*************************************************/

    @RequestMapping(value = "permission", method = RequestMethod.GET)
    public String showPermissionMangeView(Model model) {
        List<SysPermission> permissionGroup = permissionService.getPermissionGroup();
        model.addAttribute("group", permissionGroup);
        return "admin/permission/permission";
    }

    @ResponseBody
    @RequestMapping(value = "permissions", method = RequestMethod.POST)
    public DataTableResult getPermissionList(HttpServletRequest request) {
        String keyword = request.getParameter("keyword");
        String groupid=request.getParameter("groupid");
        Page<Permission> pageBean = getPageBean(request);

        DataTableResult<Permission> dataTableResult = new DataTableResult<>();
        return DatatableUtils.getDataTableResult(dataTableResult, permissionService.getPageListModel(pageBean, keyword,groupid));
    }

    /**
     * 获取权限分组树
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "permission/gtree", method = RequestMethod.POST)
    public List<TreeNodeSimple> getPermissinGTree(HttpServletRequest request) {
        return permissionService.getPermissionGroupTree();
    }

    @ResponseBody
    @RequestMapping(value = "permission/tree", method = RequestMethod.POST)
    public List<TreeNodeSimple> getPermissinTree(HttpServletRequest request) {
        return permissionService.getSimpleTreeNodes();
    }


    @RequestMapping(value = "permission/add", method = RequestMethod.GET)
    public String showPermissionAddView() {
        return "admin/permission/permission_edit";
    }


    @RequestMapping(value = "permission/edit", method = RequestMethod.GET)
    public String showPermissionEditView(Model model, HttpServletRequest request) {
        String id = request.getParameter("id");
        SysPermission permissionByID = permissionService.getPermissionByID(id);
        model.addAttribute("info", permissionByID);
        return "admin/permission/permission_edit";
    }

    @ResponseBody
    @RequestMapping(value = "permission/edit", method = RequestMethod.POST)
    public Result showPermissionEditView(SysPermission sysPermission, HttpServletRequest request) {
        return getCommonDataResult(permissionService.editPermission(sysPermission));
    }

    @ResponseBody
    @RequestMapping(value = "permission/del", method = RequestMethod.POST)
    public Result delPermission(@RequestParam("list[]") List<String> ids) {

        Result result = new Result();
        result.setMessage(permissionService.delPermissions(ids));
        result.setSuccess(true);
        return result;
    }

    /**********************************角色相关*************************************************/
    @RequestMapping(value = "role", method = RequestMethod.GET)
    public String showRoleMangeView() {
        return "admin/role/role";
    }

    @ResponseBody
    @RequestMapping(value = "role/list", method = RequestMethod.POST)
    public DataTableResult getRoleList(HttpServletRequest request) {
        String keyword = request.getParameter("keyword");
        Page<SysRole> pageBean = getPageBean(request);

        DataTableResult<SysPermission> dataTableResult = new DataTableResult<>();
        return DatatableUtils.getDataTableResult(dataTableResult, roleService.getPageList(pageBean, keyword));
    }

    @RequestMapping(value = "role/add", method = RequestMethod.GET)
    public String showRoleAddView() {
        return "admin/role/role_edit";
    }

    @RequestMapping(value = "role/edit", method = RequestMethod.GET)
    public String showRoleEditView(Model model, HttpServletRequest request) {
        String id = request.getParameter("id");
        SysRole sysRole = roleService.getRoleByID(id);
        model.addAttribute("info", sysRole);
        return "admin/role/role_edit";
    }

    @ResponseBody
    @RequestMapping(value = "role/edit", method = RequestMethod.POST)
    public Result editRole(SysRole sysRole, HttpServletRequest request) {
        return getCommonDataResult(roleService.editRole(sysRole));
    }

    @ResponseBody
    @RequestMapping(value = "role/del", method = RequestMethod.POST)
    public Result delRole(@RequestParam("list[]") List<String> ids) {
        Result result = new Result();
        result.setMessage(roleService.delRoles(ids));
        result.setSuccess(true);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "role/stree", method = RequestMethod.POST)
    public List<TreeNodeSimple> getPermissinSimpleTree(HttpServletRequest request) {
        String roleID = request.getParameter("id");
        return roleService.getPermissionTree(roleID);
    }

    @ResponseBody
    @RequestMapping(value = "role/changepermission", method = RequestMethod.POST)
    public Result changeRolePermission(HttpServletRequest request) {
        String roleID = request.getParameter("rid");
        String permissionID = request.getParameter("pid");
        boolean checked = Boolean.parseBoolean(request.getParameter("checked"));
        boolean isGroup = Boolean.parseBoolean(request.getParameter("isGroup"));

        if (isGroup)
            roleService.changeRolePermissionGroup(roleID, permissionID, checked);
        else
            roleService.changeRolePermission(roleID, permissionID, checked);

        return getCommonDataResult(true);
    }
}
