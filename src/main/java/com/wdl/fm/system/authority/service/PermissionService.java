package com.wdl.fm.system.authority.service;

import com.wdl.fm.common.entity.tree.TreeNode;
import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.SysPermission;
import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.authority.bo.Permission;
import com.wdl.fm.system.menu.bo.Menu;

import java.util.List;

/**
 * Created by Administrator on 2017/6/6.
 */
public interface PermissionService {
    /**
     * 获取所有权限
     *
     * @return
     */
    List<SysPermission> getAllPermissions();

    /**
     * 通过id 获取权限
     *
     * @param id
     * @return
     */
    SysPermission getPermissionByID(String id);

    /**
     * 获取分页数据
     *
     * @param page
     * @return
     */
    Page<SysPermission> getPageList(Page<SysPermission> page, String keyword,String groupid);


    Page<Permission> getPageListModel(Page<Permission> page, String keyword, String groupid);

    /**
     * 获取当前层的树节点
     *
     * @param id 如果为空 则获取最上层的树节点
     * @return
     */
    List<TreeNode> getTreeNodesByID(String id);

    /**
     * 判断该id 是否还有孩子
     *
     * @param id
     * @return
     */
    boolean hasChilds(String id);

    /**
     * 添加权限
     *
     * @param permission
     * @return
     */
    boolean addPermission(SysPermission permission);

    /**
     * 更新权限
     *
     * @param permission
     * @return
     */
    boolean updatePermission(SysPermission permission);

    /**
     * 编辑权限 若不存在则添加
     *
     * @param permission
     * @return
     */
    boolean editPermission(SysPermission permission);

    /**
     * 删除权限
     *
     * @param id
     * @return
     */
    boolean delPermission(String id);

    /**
     * 删除当前权限及以下子权限
     * @param id
     * @return
     */
    int delPermissionAndChild(String id);

    /**
     * 删除权限
     *
     * @param ids
     * @return 删除的数据描述
     */
    String delPermissions(List<String> ids);

    /**
     * 获取权限的所有节点
     *
     * @return
     */
    List<TreeNodeSimple> getSimpleTreeNodes();

    /**
     * 获取分组下的权限
     *
     * @param pid
     * @return
     */
    List<SysPermission> getPermissionByPid(String pid);

    /**
     * 获取权限分组
     * @return
     */
    List<SysPermission> getPermissionGroup();

    /**
     * 获取权限分组的tree
     * @return
     */
    List<TreeNodeSimple> getPermissionGroupTree();
}
