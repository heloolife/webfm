package com.wdl.fm.system.menu.service;

import com.wdl.fm.common.entity.tree.TreeNode;
import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.SysMenu;
import com.wdl.fm.common.model.SysMenuPermission;
import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.menu.bo.Menu;
import com.wdl.fm.system.menu.bo.MenuModel;

import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */
public interface MenuService {

    SysMenu getSysMenuByID(String id);
    /**
     * 获取菜单
     * @return
     */
    Menu getMenu();

    /**
     * 获取menu对应的权限
     * @param id
     * @return
     */
    String getPermissionIDByMenuID(String id);

    /**
     * 获取子节点
     * @param id
     * @return
     */
    List<SysMenu> getChildsByID(String id);

    Menu getMenuByID(String id);

    Page<SysMenu> getPageList(Page<SysMenu> page, String keyword);

    Page<MenuModel> getPageListExtend(Page<MenuModel> page, String keyword);

    /**
     * 获取层级菜单分组
     * @param id
     * @return
     */
    List<TreeNode> getMenuGroupLevel(String id);

    /**
     * 是否属于菜单分组
     * @param menu
     * @return
     */
    boolean isMenuGroup(SysMenu menu);

    /**
     * 获取菜单分级树
     * @return
     */
    List<TreeNodeSimple> getMenuGroupTree();

    /**
     * 是否还有子菜单分组
     * @param id
     * @return
     */
    boolean hasChildsGroup(String id);

    /**
     * 编辑menu
     * @param menu
     * @return
     */
    boolean editMenu(SysMenu menu);
    boolean addMenu(SysMenu menu);
    boolean updateMenu(SysMenu menu);

    boolean delMenu(String id);

    String delMenus(List<String> ids);

    /**
     * 菜单赋权
     * @param menuID
     * @param permissionID
     * @return
     */
    boolean empowerMenu(String menuID,String permissionID);

    /**
     * 菜单是否已经有权限
     * @param menuID
     * @return
     */
    boolean hasMenuPermission(String menuID);

    /**
     * 删除菜单权限
     * @param menuID
     * @return
     */
    boolean delMenuPermission(String menuID);

    /**
     * 获取菜单对应权限
     * @param menuID
     * @return
     */
    SysMenuPermission getMenuPermission(String menuID);

    /**
     * 获取menu
     * @param id
     * @return
     */
    MenuModel getMenuModelByID(String id);

}
