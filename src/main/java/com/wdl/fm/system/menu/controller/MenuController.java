package com.wdl.fm.system.menu.controller;

import com.wdl.fm.common.controller.BaseController;
import com.wdl.fm.common.entity.DataTableResult;
import com.wdl.fm.common.entity.Result;
import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.SysMenu;
import com.wdl.fm.common.utils.DatatableUtils;
import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.menu.bo.Menu;
import com.wdl.fm.system.menu.bo.MenuModel;
import com.wdl.fm.system.menu.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/6/12.
 */
@Controller
@RequestMapping("/admin/menu")
public class MenuController extends BaseController {
    @Autowired
    private MenuService menuService;

    /**
     * menu管理界面
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String showMangeMenuView() {
        return "admin/menu/menu";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String showMenuAdd() {
        return "admin/menu/menu_edit";
    }

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String showMenuEdit(Model model, HttpServletRequest request) {
        String id = request.getParameter("id");
        model.addAttribute("info", menuService.getMenuModelByID(id));
        return "admin/menu/menu_edit";
    }

    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    public Result delMenu(@RequestParam("list[]") List<String> ids) {
        Result result = new Result();
        result.setSuccess(true);
        result.setMessage(menuService.delMenus(ids));
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "empower", method = RequestMethod.POST)
    public Result delMenu(HttpServletRequest request) {
        String menuID = request.getParameter("mid");
        String permissionID = request.getParameter("pid");

        Result result = new Result();
        result.setSuccess(menuService.empowerMenu(menuID, permissionID));
        result.setMessage("");
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "upmenu", method = RequestMethod.POST)
    public Result menuEdit(SysMenu menu, HttpServletRequest request) {
        Result result = new Result();
        result.setSuccess(false);
        result.setMessage("信息修改失败");
        if (menuService.editMenu(menu)) {
            result.setMessage("信息修改成功");
            result.setSuccess(true);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "gtree", method = RequestMethod.POST)
    public List<TreeNodeSimple> getGroupLevel(HttpServletRequest request) {
        return menuService.getMenuGroupTree();
    }

    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public DataTableResult getMenuList(HttpServletRequest request) {
        String keyword = request.getParameter("keyword");
        Page<MenuModel> pageBean = getPageBean(request);

        DataTableResult<MenuModel> dataTableResult = new DataTableResult<>();
        return DatatableUtils.getDataTableResult(dataTableResult, menuService.getPageListExtend(pageBean, keyword));
    }
}
