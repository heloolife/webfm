package com.wdl.fm.system.menu.bo;

import com.wdl.fm.common.model.SysMenu;

/**
 * Created by Administrator on 2017/6/14.
 */
public class MenuModel extends SysMenu {
    private String parentName;

    private String permissionName;

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }
}
