package com.wdl.fm.system.menu.bo;

import com.wdl.fm.common.model.SysMenu;
import com.wdl.fm.common.model.SysPermission;

import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 * 菜单
 */
public class Menu extends SysMenu {
    private SysPermission permission;
    private List<Menu> childs;

    public SysPermission getPermission() {
        return permission;
    }

    public void setPermission(SysPermission permission) {
        this.permission = permission;
    }

    public List<Menu> getChilds() {
        return childs;
    }

    public void setChilds(List<Menu> childs) {
        this.childs = childs;
    }
}
