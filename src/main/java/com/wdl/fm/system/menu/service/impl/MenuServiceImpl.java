package com.wdl.fm.system.menu.service.impl;

import com.wdl.fm.common.dao.SysMenuMapper;
import com.wdl.fm.common.dao.SysMenuPermissionMapper;
import com.wdl.fm.common.entity.tree.TreeNode;
import com.wdl.fm.common.entity.tree.TreeNodeSimple;
import com.wdl.fm.common.model.*;
import com.wdl.fm.common.utils.ApplicationUtils;
import com.wdl.fm.common.utils.BeanUtil;
import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.authority.service.PermissionService;
import com.wdl.fm.system.menu.bo.Menu;
import com.wdl.fm.system.menu.bo.MenuModel;
import com.wdl.fm.system.menu.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private SysMenuMapper menuMapper;
    @Autowired
    private SysMenuPermissionMapper sysMenuPermissionMapper;

    @Override
    public SysMenu getSysMenuByID(String id) {
        return menuMapper.selectByPrimaryKey(id);
    }

    @Override
    public Menu getMenu() {
        Menu menu = getMenu(null);
        return menu;
    }

    @Override
    public String getPermissionIDByMenuID(String id) {
        SysMenuPermissionExample example = new SysMenuPermissionExample();
        example.createCriteria().andMenuIdEqualTo(id);
        List<SysMenuPermission> sysMenuPermissions = sysMenuPermissionMapper.selectByExample(example);
        if (sysMenuPermissions != null && sysMenuPermissions.size() > 0)
            return sysMenuPermissions.get(0).getPermissionId();
        return null;
    }

    @Override
    public List<SysMenu> getChildsByID(String id) {
        SysMenuExample example = new SysMenuExample();
        SysMenuExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause("sort asc");
        if (id == null)
            criteria.andPidEqualTo("");
        else
            criteria.andPidEqualTo(id);
        return menuMapper.selectByExample(example);
    }

    @Override
    public Menu getMenuByID(String id) {
        Menu menu = new Menu();

        SysMenu sysMenu = menuMapper.selectByPrimaryKey(id);

        if (sysMenu != null)
            BeanUtil.copyProperties(sysMenu, menu);

        return menu;
    }

    @Override
    public Page<SysMenu> getPageList(Page<SysMenu> page, String keyword) {
        if (keyword == null)
            keyword = "";
        SysMenuExample example = new SysMenuExample();
        example.createCriteria().andNameLike("%" + keyword + "%");

        page.setResult(menuMapper.selectByExample(page, example));

        return page;
    }

    @Override
    public Page<MenuModel> getPageListExtend(Page<MenuModel> page, String keyword) {

        if (keyword == null)
            keyword = "";
        SysMenuExample example = new SysMenuExample();
        example.createCriteria().andNameLike("%" + keyword + "%");

        List<MenuModel> menuModels = menuMapper.selectByExampleModel(example);
        SysMenuPermission menuPermission;
        SysPermission permissionByID;
        SysMenu sysMenuByID;
        for (MenuModel model : menuModels) {
            //设置父节点
            if (model.getPid() != null && !"".equals(model.getPid())) {
                sysMenuByID = getSysMenuByID(model.getPid());
                model.setParentName(sysMenuByID != null ? sysMenuByID.getName() : "");
            }
            //设置权限名称
            menuPermission = getMenuPermission(model.getId());
            if (menuPermission != null) {
                permissionByID = permissionService.getPermissionByID(menuPermission.getPermissionId());
                if (permissionByID != null)
                    model.setPermissionName(permissionByID.getName());
            }
        }

        page.setResult(menuModels);

        return page;
    }

    @Override
    public List<TreeNode> getMenuGroupLevel(String id) {
        List<TreeNode> treeNodes = new ArrayList<>();

        SysMenuExample example = new SysMenuExample();
        example.createCriteria().andPidEqualTo(id == null ? "" : id).andUrlIsNull();
        if (id == null || "".equals(id))
            example.or().andPidIsNull();

        List<SysMenu> sysMenus = menuMapper.selectByExample(example);
        TreeNode node;
        for (SysMenu menu : sysMenus) {
            if (isMenuGroup(menu)) {
                node = new TreeNode();
                node.setName(menu.getName());
                node.setId(menu.getId());
                node.setIsParent(hasChildsGroup(menu.getId()));

                treeNodes.add(node);
            }
        }
        return treeNodes;
    }

    @Override
    public boolean isMenuGroup(SysMenu menu) {
        return menu.getUrl() == null || menu.getUrl().equals("");
    }

    @Override
    public List<TreeNodeSimple> getMenuGroupTree() {
        List<TreeNodeSimple> tree = new ArrayList<>();

        List<SysMenu> sysMenus = menuMapper.selectByExample(new SysMenuExample());
        TreeNodeSimple node;
        for (SysMenu menu : sysMenus) {
            node = new TreeNodeSimple();

            node.setId(menu.getId());
            node.setpId(menu.getPid());
            node.setName(menu.getName());
            node.setIsParent(hasChildsGroup(menu.getId()));

            tree.add(node);
        }

        return tree;
    }

    @Override
    public boolean hasChildsGroup(String id) {
        SysMenuExample example = new SysMenuExample();
        example.createCriteria().andPidEqualTo(id);
        return menuMapper.selectByExample(example).size() > 0;
    }

    @Override
    public boolean addMenu(SysMenu menu) {
        menu.setId(ApplicationUtils.getUUID());
        return menuMapper.insert(menu) > 0;
    }

    @Override
    public boolean updateMenu(SysMenu menu) {
        return menuMapper.updateByPrimaryKey(menu) > 0;
    }

    @Override
    public boolean delMenu(String id) {
        return menuMapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public String delMenus(List<String> ids) {
        int count = 0;
        for (String id : ids) {
            if (delMenu(id))
                count++;
        }
        return "成功删除" + count + "条数据，失败" + (ids.size() - count) + "条";
    }

    @Override
    public boolean empowerMenu(String menuID, String permissionID) {
        if (hasMenuPermission(menuID))
            delMenuPermission(menuID);

        SysMenuPermission menuPermission = new SysMenuPermission();
        menuPermission.setMenuId(menuID);
        menuPermission.setPermissionId(permissionID);
        return sysMenuPermissionMapper.insert(menuPermission) > 0;
    }

    @Override
    public boolean hasMenuPermission(String menuID) {
        SysMenuPermissionExample example = new SysMenuPermissionExample();
        example.createCriteria().andMenuIdEqualTo(menuID);
        return sysMenuPermissionMapper.selectByExample(example).size() > 0;
    }

    @Override
    public boolean delMenuPermission(String menuID) {
        SysMenuPermissionExample example = new SysMenuPermissionExample();
        example.createCriteria().andMenuIdEqualTo(menuID);
        return sysMenuPermissionMapper.deleteByExample(example) > 0;
    }

    @Override
    public SysMenuPermission getMenuPermission(String menuID) {
        return sysMenuPermissionMapper.selectByPrimaryKey(menuID);
    }

    @Override
    public MenuModel getMenuModelByID(String id) {
        SysMenu menu = getSysMenuByID(id);
        MenuModel menuModel = new MenuModel();

        if (menu != null) {
            BeanUtil.copyProperties(menu, menuModel);
            if (menuModel.getPid() != null || !"".equals(menuModel.getPid()))
                menuModel.setParentName(getSysMenuByID(menuModel.getPid()).getName());
        }
        return menuModel;
    }

    @Override
    public boolean editMenu(SysMenu menu) {
        if (menu.getId() == null || "".equals(menu.getId()))
            return addMenu(menu);
        else
            return updateMenu(menu);
    }

    private Menu getMenu(String id) {
        List<Menu> childs = new ArrayList<>();
        List<SysMenu> childsByID = getChildsByID(id);
        for (SysMenu info : childsByID) {
            childs.add(getMenu(info.getId()));
        }

        Menu menu = getMenuByID(id);
        if (id != null)
            menu.setPermission(permissionService.getPermissionByID(getPermissionIDByMenuID(id)));
        menu.setChilds(childs);

        return menu;
    }
}
