package com.wdl.fm.common.dao;

import com.wdl.fm.common.model.SysMenu;
import com.wdl.fm.common.model.SysMenuExample;
import java.util.List;

import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.menu.bo.MenuModel;
import org.apache.ibatis.annotations.Param;

public interface SysMenuMapper {
    int countByExample(SysMenuExample example);

    int deleteByExample(SysMenuExample example);

    int deleteByPrimaryKey(String id);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    List<SysMenu> selectByExample(SysMenuExample example);

    List<MenuModel> selectByExampleModel(SysMenuExample example);

    List<SysMenu> selectByExample(Page<SysMenu> page,SysMenuExample example);

    SysMenu selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") SysMenu record, @Param("example") SysMenuExample example);

    int updateByExample(@Param("record") SysMenu record, @Param("example") SysMenuExample example);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKey(SysMenu record);
}