package com.wdl.fm.common.dao;

import com.wdl.fm.common.model.SysPermission;
import com.wdl.fm.common.model.SysPermissionExample;

import java.util.List;

import com.wdl.fm.core.orm.mybatis.Page;
import com.wdl.fm.system.authority.bo.Permission;
import org.apache.ibatis.annotations.Param;

public interface SysPermissionMapper {
    int countByExample(SysPermissionExample example);

    int deleteByExample(SysPermissionExample example);

    int deleteByPrimaryKey(String id);

    int insert(SysPermission record);

    int insertSelective(SysPermission record);

    List<SysPermission> selectByExample(SysPermissionExample example);

    List<SysPermission> selectByExample(Page<SysPermission> page, SysPermissionExample example);

    List<Permission> selectByExampleModel(Page<Permission> page, SysPermissionExample example);

    SysPermission selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") SysPermission record, @Param("example") SysPermissionExample example);

    int updateByExample(@Param("record") SysPermission record, @Param("example") SysPermissionExample example);

    int updateByPrimaryKeySelective(SysPermission record);

    int updateByPrimaryKey(SysPermission record);
}