package com.wdl.fm.common.dao;

import com.wdl.fm.common.model.SysMenuPermission;
import com.wdl.fm.common.model.SysMenuPermissionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysMenuPermissionMapper {
    int countByExample(SysMenuPermissionExample example);

    int deleteByExample(SysMenuPermissionExample example);

    int deleteByPrimaryKey(String menuId);

    int insert(SysMenuPermission record);

    int insertSelective(SysMenuPermission record);

    List<SysMenuPermission> selectByExample(SysMenuPermissionExample example);

    SysMenuPermission selectByPrimaryKey(String menuId);

    int updateByExampleSelective(@Param("record") SysMenuPermission record, @Param("example") SysMenuPermissionExample example);

    int updateByExample(@Param("record") SysMenuPermission record, @Param("example") SysMenuPermissionExample example);

    int updateByPrimaryKeySelective(SysMenuPermission record);

    int updateByPrimaryKey(SysMenuPermission record);
}