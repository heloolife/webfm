package com.wdl.fm.common.entity;

/**
 * Created by Administrator on 2017/6/8.
 */
public class Result {
    private boolean success;
    private String message;
    private int code;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
