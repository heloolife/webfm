package com.wdl.fm.common.entity;

import java.util.List;

/**
 * Created by Administrator on 2017/6/8.
 */
public class DataTableResult<T> extends Result {
    private int recordsTotal;
    private int recordsFiltered;
    private List<T> data;


    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
