package com.wdl.fm.common.entity;

import java.util.List;

/**
 * Created by Administrator on 2017/6/12.
 */
public class RequestListCommon<T> {
    private List<T> list;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
