package com.wdl.fm.common.entity.tree;

/**
 * Created by Administrator on 2017/6/9.
 */
public class TreeNode {
    private String id;
    private String name;
    private boolean isParent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(boolean parent) {
        isParent = parent;
    }
}
