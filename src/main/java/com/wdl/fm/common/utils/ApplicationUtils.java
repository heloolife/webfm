package com.wdl.fm.common.utils;

import java.util.UUID;
import org.apache.commons.codec.digest.DigestUtils;
/**
 * Created by Administrator on 2017/6/6.
 */
public class ApplicationUtils {
    public ApplicationUtils() {
    }

    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }

    public static String md5Hex(String value) {
        return DigestUtils.md5Hex(value);
    }

    public static String sha1Hex(String value) {
        return DigestUtils.sha1Hex(value);
    }

    public static String sha256Hex(String value) {
        return DigestUtils.sha256Hex(value);
    }

    public static String getUUID() {
        String strUUID = randomUUID();
        return strUUID.replaceAll("-", "");
    }
}
