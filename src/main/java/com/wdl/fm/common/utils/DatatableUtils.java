package com.wdl.fm.common.utils;

import com.wdl.fm.common.entity.DataTableResult;
import com.wdl.fm.core.orm.mybatis.Page;

/**
 * Created by Administrator on 2017/6/8.
 */
public class DatatableUtils {
    public DatatableUtils() {
    }

    public static DataTableResult getDataTableResult(DataTableResult dataTableResult, Page page) {
        if(null != page) {
            dataTableResult.setSuccess(true);
            dataTableResult.setCode(200);
            dataTableResult.setData(page.getResult());
            dataTableResult.setRecordsTotal(page.getTotalCount());
            dataTableResult.setRecordsFiltered(page.getTotalCount());
        }

        return dataTableResult;
    }


}
