package com.wdl.fm.common.controller;

import com.wdl.fm.common.constants.GlobalConstant;
import com.wdl.fm.common.model.SysUser;
import com.wdl.fm.system.menu.bo.Menu;
import com.wdl.fm.system.menu.service.MenuService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


/**
 * Created by Administrator on 2017/5/24.
 */
@Controller
@RequestMapping("/admin")
public class CommonController {
    @Autowired
    private MenuService menuService;

    @RequestMapping("/index")
    public String showIndex(Model model) {
        Menu menu = menuService.getMenu();
        model.addAttribute("menu",menu);

        Subject subject = SecurityUtils.getSubject();
        String s = subject.getPrincipal().toString();

        model.addAttribute("username",subject.getPrincipal().toString());
        return "admin/index";
    }


}
