package com.wdl.fm.common.controller;

import com.wdl.fm.common.entity.Result;
import com.wdl.fm.core.orm.mybatis.Page;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2017/5/24.
 */
public class BaseController {
    public Page getPageBean(HttpServletRequest request) {
        String sEcho = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        Integer startInt = 0;
        Integer lengthInt = 10;

        if (start != null && !"".equals(start)) {
            startInt = new Integer(start);
        }
        if (start != null && !"".equals(length)) {
            if ("-1".equals(length)) {
                lengthInt = Integer.MAX_VALUE;
            } else {
                lengthInt = new Integer(length);
            }
        }
        return new Page(sEcho, startInt, lengthInt);
    }

    protected Result getCommonDataResult(boolean success) {
        Result result = new Result();
        result.setSuccess(success);
        result.setMessage("数据更新失败");
        if (success)
            result.setMessage("数据更新成功");

        return result;
    }
}
