package com.wdl.fm.common.exception;

/**
 * Created by Administrator on 2017/6/6.
 */
public class LoginException extends Exception {
    public LoginException(String msg) {
        super(msg);
    }
}
