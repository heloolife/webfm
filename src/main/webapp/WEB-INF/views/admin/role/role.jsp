<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>权限列表</title>

    <%@include file="/WEB-INF/views/admin/include/include_compatible_head_set.jsp" %>
    <%@include file="/WEB-INF/views/admin/include/include_hui_css.jsp" %>
    <%@include file="/WEB-INF/views/admin/include/include_ztree_css.jsp" %>

</head>
<body>
<%@include file="/WEB-INF/views/admin/include/include_page_header.jsp" %>

<div class="page-container">
    <div class="cl pd-5 bg-1 bk-gray">
		<span class="l">
		<a href="javascript:;" onclick="delGroup()" class="btn btn-danger radius">
            <i class="Hui-iconfont">&#xe6e2;</i> 批量删除
        </a>
		<a class="btn btn-primary radius" href="javascript:;" onclick="addInfo()">
            <i class="Hui-iconfont">&#xe600;</i> 添加角色
        </a>
</span>
        <span class="r">
        <input type="text" class="input-text " style="width:250px" placeholder="输入关键词" id="keyword" name=""/>
        <button type="submit" class="btn btn-success radius " onclick="search()">
            <i class="Hui-iconfont">&#xe665;</i> 搜索
        </button>
</span>
    </div>
    <table id="dtable" class="table table-border table-bordered table-bg  table-sort">
        <thead>
        <tr class="text-c">
            <th width="25"><input type="checkbox" value="" name=""></th>
            <th>角色名称</th>
            <th>角色标识</th>
            <th>创建时间</th>
            <th>更新时间</th>
            <th>角色描述</th>
            <th>操作</th>
        </tr>
        </thead>
    </table>
</div>

<div id="treeContent" style="display: none;">
    <ul id="tree" class="ztree" style="margin-top:0; width:210px;"></ul>
</div>

<%@include file="/WEB-INF/views/admin/include/include_hui_js.jsp" %>
<%@include file="/WEB-INF/views/admin/include/include_datatable_js.jsp" %>
<%@include file="/WEB-INF/views/admin/include/include_ztree_js.jsp" %>

<script src="<c:url value="/app/js/admin/role/role.js" />"></script>

<script>


    Role.init();

    function search() {
        Role.search();
    }
    function delGroup() {
        Role.delGroup();
    }

    function addInfo() {
        Role.addinfo();
    }

</script>

</body>
</html>