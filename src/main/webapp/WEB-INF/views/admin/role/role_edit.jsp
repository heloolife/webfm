<%@ page import="com.wdl.fm.common.model.SysRole" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="Bookmark" href="/favicon.ico">
    <link rel="Shortcut Icon" href="/favicon.ico"/>
    <title>菜单信息操作</title>

    <%@include file="/WEB-INF/views/admin/include/include_compatible_head_set.jsp" %>
    <%@include file="/WEB-INF/views/admin/include/include_hui_css.jsp" %>

</head>
<%
    SysRole role = (SysRole) request.getAttribute("info");
%>
<body>
<article class="page-container">
    <form class="form form-horizontal" id="form-add">

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>角色名称：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" hidden id="id" name="id" value="<%=role!=null?role.getId():""%>">
                <input type="text" class="input-text" value="<%=role!=null?role.getRolename():""%>"
                       id="rolename" name="rolename">
            </div>
        </div>


        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>角色标识：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="<%=role!=null?role.getRolesign():""%>"
                       id="rolesign" name="rolesign">
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">角色描述：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <textarea id="description" name="description" class="textarea"
                          placeholder="说点什么...100个字符以内"
                          dragonfly="true"><%=role != null ? role.getDescription() : ""%></textarea>
            </div>
        </div>

        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                <button class="btn btn-primary radius" type="submit">&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
            </div>
        </div>
    </form>
</article>

<%@include file="/WEB-INF/views/admin/include/include_hui_js.jsp" %>
<%@include file="/WEB-INF/views/admin/include/include_validate_js.jsp" %>

<script src="<c:url value="/app/js/admin/role/role_edit.js" />"></script>

<script type="text/javascript">
    RoleEdit.init();
</script>
</body>
</html>