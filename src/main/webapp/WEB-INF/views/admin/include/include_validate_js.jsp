<!-- 数据验证的相关js -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="<c:url value="/assets/plugins/jquery.validation/1.14.0/jquery.validate.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/plugins/jquery.validation/1.14.0/validate-methods.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/plugins/jquery.validation/1.14.0/messages_zh.js"/>"></script>
