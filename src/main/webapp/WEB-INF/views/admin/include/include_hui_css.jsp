<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/assets/h-ui/h-ui/css/H-ui.min.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/assets/h-ui/h-ui.admin/css/style.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/assets/plugins/Hui-iconfont/1.0.8/iconfont.css"/>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/h-ui/h-ui.admin/css/H-ui.admin.css"/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/h-ui/h-ui.admin/skin/default/skin.css"/>"
      id="skin"/>