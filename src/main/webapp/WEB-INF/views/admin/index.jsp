<%@ page import="com.wdl.fm.system.menu.bo.Menu" %>
<%@ page import="com.wdl.fm.common.model.SysUser" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="Bookmark" href="/favicon.ico">
    <link rel="Shortcut Icon" href="/favicon.ico"/>
    <title>H-ui.admin v3.0</title>

    <%@include file="/WEB-INF/views/admin/include/include_compatible_head_set.jsp" %>
    <%@include file="/WEB-INF/views/admin/include/include_hui_css.jsp" %>
</head>

<%
    Menu menu = (Menu) request.getAttribute("menu");
    String username = (String) request.getAttribute("username");
%>
<body>
<header class="navbar-wrapper">
    <div class="navbar navbar-fixed-top">
        <div class="container-fluid cl"><a class="logo navbar-logo f-l mr-10 hidden-xs" href="/aboutHui.shtml">H-ui.admin</a>
            <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/aboutHui.shtml">H-ui</a>
            <span class="logo navbar-slogan f-l mr-10 hidden-xs">v3.0</span>
            <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
            <nav class="nav navbar-nav">
                <ul class="cl">
                    <li class="dropDown dropDown_hover"><a href="javascript:;" class="dropDown_A"><i
                            class="Hui-iconfont">&#xe600;</i> 新增 <i class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" onclick="article_add('添加资讯','article-add.html')"><i
                                    class="Hui-iconfont">&#xe616;</i> 资讯</a></li>
                            <li><a href="javascript:;" onclick="picture_add('添加资讯','picture-add.html')"><i
                                    class="Hui-iconfont">&#xe613;</i> 图片</a></li>
                            <li><a href="javascript:;" onclick="product_add('添加资讯','product-add.html')"><i
                                    class="Hui-iconfont">&#xe620;</i> 产品</a></li>
                            <li><a href="javascript:;" onclick="member_add('添加用户','member-add.html','','510')"><i
                                    class="Hui-iconfont">&#xe60d;</i> 用户</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
                <ul class="cl">
                    <li class="dropDown dropDown_hover">
                        <a href="#" class="dropDown_A"><%=username != null ? username : ""%> <i
                                class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="<%=request.getContextPath()%>/page/admin/login/logout">退出</a></li>
                        </ul>
                    </li>
                    <li id="Hui-msg"><a href="#" title="消息"><span class="badge badge-danger">1</span><i
                            class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a></li>
                    <li id="Hui-skin" class="dropDown right dropDown_hover"><a href="javascript:;" class="dropDown_A"
                                                                               title="换肤"><i class="Hui-iconfont"
                                                                                             style="font-size:18px">&#xe62a;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
                            <li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
                            <li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
                            <li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
                            <li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
                            <li><a href="javascript:;" data-val="orange" title="橙色">橙色</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<aside class="Hui-aside">
    <div class="menu_dropdown bk_2">

        <!-- 在此只使用二级菜单 -->
        <% for (Menu info : menu.getChilds()) {
            if (info.getPermission() != null) {
        %>
        <shiro:hasPermission name="<%=info.getPermission().getSign()%>">

            <dl id="<%=info.getId()%>">
                <dt>
                    <i class="Hui-iconfont"><%=info.getIcon()%></i> <%=info.getName()%>
                    <i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
                </dt>
                <dd>
                    <ul>
                        <% for (Menu schild : info.getChilds()) {
                            if (schild.getPermission() != null) {
                        %>
                        <shiro:hasPermission name="<%=schild.getPermission().getSign()%>">
                            <li>
                                <a data-href="<%=request.getContextPath()+schild.getUrl()%>"
                                   data-title="<%=schild.getName()%>"
                                   href="javascript:void(0)"><%=schild.getName()%>
                                </a>
                            </li>
                        </shiro:hasPermission>

                        <%} else {%>

                        <li>
                            <a data-href="<%=request.getContextPath()+schild.getUrl()%>"
                               data-title="<%=schild.getName()%>"
                               href="javascript:void(0)"><%=schild.getName()%>
                            </a>
                        </li>

                        <%
                                }
                            }
                        %>
                    </ul>
                </dd>
            </dl>

        </shiro:hasPermission>
        <% } else { %>
        <dl id="<%=info.getId()%>">
            <dt>
                <i class="Hui-iconfont">&#xe616;</i> <%=info.getName()%>
                <i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
            </dt>
            <dd>
                <ul>
                    <% for (Menu schild : info.getChilds()) {
                        if (schild.getPermission() != null) {
                    %>
                    <shiro:hasPermission name="<%=schild.getPermission().getSign()%>">
                        <li>
                            <a data-href="<%=request.getContextPath()+schild.getUrl()%>"
                               data-title="<%=schild.getName()%>"
                               href="javascript:void(0)"><%=schild.getName()%>
                            </a>
                        </li>
                    </shiro:hasPermission>

                    <%} else {%>

                    <li>
                        <a data-href="<%=request.getContextPath()+schild.getUrl()%>"
                           data-title="<%=schild.getName()%>"
                           href="javascript:void(0)"><%=schild.getName()%>
                        </a>
                    </li>

                    <%
                            }
                        }
                    %>
                </ul>
            </dd>
        </dl>
        <% }
        } %>

    </div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a>
</div>
<section class="Hui-article-box">
    <div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
        <div class="Hui-tabNav-wp">
            <ul id="min_title_list" class="acrossTab cl">
                <li class="active">
                    <span title="我的桌面" data-href="welcome.html">我的桌面</span>
                    <em></em></li>
            </ul>
        </div>
        <div class="Hui-tabNav-more btn-group"><a id="js-tabNav-prev" class="btn radius btn-default size-S"
                                                  href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a><a
                id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a>
        </div>
    </div>
    <div id="iframe_box" class="Hui-article">
        <div class="show_iframe">
            <div style="display:none" class="loading"></div>
            <iframe scrolling="yes" frameborder="0" src="welcome.html"></iframe>
        </div>
    </div>
</section>

<div class="contextMenu" id="Huiadminmenu">
    <ul>
        <li id="closethis">关闭当前</li>
        <li id="closeall">关闭全部</li>
    </ul>
</div>

<%@include file="/WEB-INF/views/admin/include/include_hui_js.jsp" %>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript"
        src="<c:url value="/assets/plugins/jquery.contextmenu/jquery.contextmenu.r2.js"/>"></script>
<script type="text/javascript">
    /*个人信息*/
    function myselfinfo() {
        layer.open({
            type: 1,
            area: ['300px', '200px'],
            fix: false, //不固定
            maxmin: true,
            shade: 0.4,
            title: '查看信息',
            content: '<div>管理员信息</div>'
        });
    }

    /*资讯-添加*/
    function article_add(title, url) {
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }
    /*图片-添加*/
    function picture_add(title, url) {
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }
    /*产品-添加*/
    function product_add(title, url) {
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }
    /*用户-添加*/
    function member_add(title, url, w, h) {
        layer_show(title, url, w, h);
    }


</script>


<script>
    (function () {

    })();
</script>
</body>
</html>