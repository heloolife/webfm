<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div id="resetpwd" style="display: none;padding: 15px">
    <label><span class="c-red">*</span>新密码：</label>

    <input type="text" class="input-text"
           placeholder="" id="password" name="password">

    <div class="btn btn-primary size-M radius" style="margin-top: 20px;position: absolute;right: 10px" onclick="rsetpwd()">修 改</div>

</div>

<script>
    function rsetpwd() {
        layer.confirm('确认要修改密码吗？', function (index) {
            //此处请求后台程序，下方是成功后的前台处理……
            layer.close(index);
            $.ajax({
                url: basePath + '/page/admin/user/rpwd',
                type: 'post',
                data: {id: czid, pwd: $('#password').val()},
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        layer.msg(data.message);
                        layer.close(czlayerindex);
                    }
                }
            })

        });
    }

</script>

