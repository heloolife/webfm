<%@ page import="com.wdl.fm.common.model.SysMenu" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="Bookmark" href="/favicon.ico">
    <link rel="Shortcut Icon" href="/favicon.ico"/>
    <title>菜单信息操作</title>

    <%@include file="/WEB-INF/views/admin/include/include_compatible_head_set.jsp" %>
    <%@include file="/WEB-INF/views/admin/include/include_hui_css.jsp" %>
    <%@include file="/WEB-INF/views/admin/include/include_ztree_css.jsp" %>

</head>
<%
    SysMenu menu = (SysMenu) request.getAttribute("info");
%>
<body>
<article class="page-container">
    <form class="form form-horizontal" id="form-add">
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">上级菜单：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" hidden id="pid" name="pid" value="<%=menu!=null?menu.getPid():""%>">
                <input type="text" class="input-text" value="" placeholder="" id="parentp" name="parentp" readonly
                       onclick="MenuEdit.showTreeMenu(true)">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>菜单名称：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" hidden id="id" name="id" value="<%=menu!=null?menu.getId():""%>">
                <input type="text" class="input-text" value="<%=menu!=null?menu.getName():""%>"
                       placeholder="" id="name" name="name">
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">菜单路径：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="<%=menu!=null?menu.getUrl():""%>"
                       placeholder="" id="url" name="url">
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">菜单图标：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="<%=menu!=null?menu.getIcon():""%>"
                       placeholder="" id="icon" name="icon">
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>菜单排序：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="<%=menu!=null?menu.getSort():""%>"
                       placeholder="" id="sort" name="sort">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">是否有效：</label>
            <div class="formControls col-xs-8 col-sm-9"> <span class="select-box" style="width:150px;">
			<select id="status" class="select" name="status" size="1">
				<option value="0"
                        selected="<%=menu!=null&&menu.getStatus().equals("0")?"selected":""%>">无效</option>
				<option value="1"
                        selected="<%=menu!=null&&menu.getStatus().equals("1")?"selected":""%>">有效</option>
			</select>
			</span></div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">菜单描述：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <textarea id="description" name="description" class="textarea"
                          placeholder="说点什么...100个字符以内"
                          dragonfly="true"><%=menu != null ? menu.getDescription() : ""%></textarea>
            </div>
        </div>

        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                <button class="btn btn-primary radius" type="submit">&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
            </div>
        </div>
    </form>
</article>
<div id="menuContent" class="menuContent"
     style="display:none; position: absolute;background: #fff;min-height: 200px;border: solid 1px #ddd">
    <ul id="permissionTree" class="ztree" style="margin-top:0; width:160px;"></ul>
</div>


<%@include file="/WEB-INF/views/admin/include/include_hui_js.jsp" %>
<%@include file="/WEB-INF/views/admin/include/include_validate_js.jsp" %>

<%@include file="/WEB-INF/views/admin/include/include_ztree_js.jsp" %>

<script src="<c:url value="/app/js/admin/menu/menu_edit.js" />"></script>

<script type="text/javascript">
    MenuEdit.init();
</script>
</body>
</html>