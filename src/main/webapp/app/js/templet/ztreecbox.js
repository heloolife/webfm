/**
 * Created by Administrator on 2017/6/14.
 * 带cbox选择工具
 */
var ZtreeCb = function () {
    var treeID = undefined;
    var treeSetting = undefined;
    var callbacks=undefined;

    function initTree() {
        treeSetting = {
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onCheck: function (event, treeId, treeNode) {

                    if (treeNode.isParent&&callbacks&&callbacks.groupChecked)
                        callbacks.groupChecked(treeNode.id, treeNode.name, treeNode.checked);
                    else if (!treeNode.isParent&&callbacks&&callbacks.childChecked)
                        callbacks.childChecked(treeNode.id, treeNode.name, treeNode.checked);

                }
            }
        };
    }


    function loadTree(data) {
        $.fn.zTree.init($("#" + treeID), treeSetting, data);
    }


    return {
        init: function (treeid) {
            treeID = treeid;
            initTree();
        },
        load: loadTree,
        addCallBack:function (callbackss) {
            callbacks=callbackss;
        }
    };

}();