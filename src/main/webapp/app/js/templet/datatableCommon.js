/**
 * Created by Administrator on 2017/6/8.
 */
var dataTableListnumber = [20, 50, -1];
var dataTableListnumbers = [20, 50, "All"];
var dataTableLengthMenu = [dataTableListnumber, dataTableListnumbers];
var dataTable_lang_zh = {
    lengthMenu: "每页显示 _MENU_ 条记录",
    zeroRecords: "抱歉， 没有找到数据",
    info: "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
    infoEmpty: "",
    infoFiltered: "(从 _MAX_ 条数据中检索)",
    filter: "过滤",
    paginate: {
        first: "首页",
        previous: "前一页",
        next: "后一页",
        last: "尾页"
    }
};

var DataTableUtil = function () {
    var tableID = undefined;
    var options = undefined;

    /**
     * 初始化datatable
     * @param tableID
     * @param columns
     * @param columnDefs
     * @param pageLength
     * @param showCheckbox
     */
    function initDataTable() {
        if (!options.pageLength)
            options.pageLength = 10;

        return $('#' + tableID).DataTable({
            destory: true,
            ordering: false,
            columns: options.columns,
            columnDefs: options.columnDefs,
            lengthMenu: dataTableLengthMenu,
            serverSide: true,
            ajax: options.ajaxconfig,
            language: dataTable_lang_zh,
            pageLength: options.pageLength,
            lengthChange: false,
            searching: false,
            info: true,
            pagingType: "full_numbers"
        });
    }


    function getCheckIds() {
        var ids = [];
        $("#" + tableID + " tbody tr").each(function () {
            var $checkbox = $(this).find("td:eq(0)").children("input[type='checkbox']");
            if ($checkbox.is(':checked')) {
                ids.push($checkbox.val());
            }
        });
        return ids;
    }

    return {
        init: function (tableid, tableOptions) {
            tableID = tableid;
            options = tableOptions;
            return initDataTable();
        },
        getChekIDs: getCheckIds
    };
}();

function getColumn(data, render) {
    return {
        data: data,
        className: 'text-c',
        render: render
    };
}

function getCheckBoxRender() {
    return function (data, type, full, meta) {
        return '<input type="checkbox"  name="" value="' + data + '" />';
    };
}