/**
 * Created by Administrator on 2017/6/12.
 */
var MenuEdit = function () {
    /**
     * 初始化权限列表
     */
    function initTree() {
        var setting = {
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onClick: function (e, treeId, treeNode) {
                    treeItemClick(treeId, treeNode);
                }
            }
        };
        $.ajax({
            url: basePath + '/page/admin/menu/gtree',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                $.fn.zTree.init($("#permissionTree"), setting, data);
            }
        });

    }

    /**
     * 是否展示treemenu
     * @param show
     */
    function showTreeMenu(show) {
        if (show) {
            var pcon = $("#parentp");
            var pconOffset = $("#parentp").offset();
            var width = $("#parentp").width() + parseInt($("#parentp").css('padding-left')) + parseInt($("#parentp").css('padding-right'));

            $("#menuContent").css({
                left: pconOffset.left + "px",
                top: pconOffset.top + pcon.outerHeight() + "px",
                width: width + 'px'
            }).slideDown("fast");

            $("body").bind("mousedown", onBodyDown);
        } else {
            $("#menuContent").fadeOut("fast");
            $("body").unbind("mousedown", onBodyDown);
        }
    }

    function onBodyDown(event) {
        if (!(event.target.id == "parentp" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length > 0)) {
            showTreeMenu(false);
        }
    }

    /**
     * 权限树节点点击
     * @param treeID
     * @param nodeInfo
     */
    function treeItemClick(treeID, nodeInfo) {
        $('#parentp').val(nodeInfo.name);
        $('#pid').val(nodeInfo.id);

        showTreeMenu(false);
    }

    /**
     * 初始化表格校验
     */
    function initValite() {
        $("#form-add").validate({
            rules: {
                name: {
                    required: true
                },
                sort: {
                    required: true,
                    number: true
                }
            },
            onkeyup: false,
            focusCleanup: true,
            focusInvalid: false,
            // success: "valid",
            submitHandler: function (form) {
                $("#form-add").ajaxSubmit({
                    url: basePath + '/page/admin/menu/upmenu',
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            parent.search();
                            layer.msg(data.message, {icon: 1, time: 1000}, function () {
                                layer_close();
                            });
                        } else {
                            layer.msg(data.message, {icon: 1, time: 1000});
                        }
                    },
                    error: function (XmlHttpRequest, textStatus, errorThrown) {
                        console.log('error:' + textStatus);
                        console.info(errorThrown);
                    }
                });

                return false;
            }
        });
    }


    return {
        init: function () {
            initTree();
            initValite();
        },
        showTreeMenu: showTreeMenu
    };
}();