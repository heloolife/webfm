/**
 * Created by Administrator on 2017/6/9.
 */
var Menu = function () {
    var dataTabel = undefined;
    var curMenuID = undefined; //单行menuID
    var curLayerIndex = undefined;


    function initDataTable() {
        var tableUrl = basePath + "/page/admin/menu/list";
        var columns = [
            getColumn("id", getCheckBoxRender()),
            getColumn("name"),
            getColumn("parentName"),
            getColumn("permissionName"),
            getColumn("icon",function (data, type, row, meta) {
                return "<i class='Hui-iconfont'>"+data+"</i>";
            }),
            getColumn("url"),
            getColumn("sort"),
            getColumn("status", function (data, type, row, meta) {
                var status = '有效';
                if (data != '1')
                    status = '失效';
                return status;
            }),
            getColumn("createTime"),
            getColumn("updateTime"),
            getColumn("description"),
            getColumn("", function (data, type, row, meta) {
                var style = 'text-decoration:none;margin-left: 4px;margin-right: 4px';

                var fq = "<a style='" + style + "' onClick='Menu.fq(\"" + row.id + "\")' href='javascript:;' title='赋权'><i class='Hui-iconfont'>&#xe61d;</i></a>";
                var edit = "<a style='" + style + "' onClick='Menu.editinfo(\"" + row.id + "\")' href='javascript:;' title='编辑'><i class='Hui-iconfont'>&#xe6df;</i></a>";
                var del = "<a style='" + style + "' class='ml-5' onClick='Menu.delItem(\"" + row.id + "\")' href='javascript:;' title='删除'><i class='Hui-iconfont'>&#xe6e2;</i></a>";

                return fq + edit + del;
            })];
        var ajaxconfig = {
            url: tableUrl,
            type: 'POST',
            data: function (d) {
                d.keyword = $("#keyword").val();
            }
        };

        dataTabel = DataTableUtil.init('dtable', {
            columns: columns,
            ajaxconfig: ajaxconfig
        });
    }

    function initTree(data) {
        var setting = {
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onClick: function (event, treeId, treeNode) {
                    if (treeNode.isParent) {
                        layer.msg('请选择具体权限！')
                        return;
                    }
                    $.ajax({
                        url: basePath + '/page/admin/menu/empower',
                        type: 'post',
                        data: {mid: curMenuID, pid: treeNode.id},
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                layer.msg("赋权成功");
                                layer.close(curLayerIndex);
                                search();
                            }
                        }
                    });
                }
            }
        };
        ztree = $.fn.zTree.init($("#tree"), setting, data);
    }

    function fq(id) {
        curMenuID = id;
        $.ajax({
            url: basePath + '/page/admin/auth/permission/tree',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                initTree(data);
                curLayerIndex = layer.open({
                    type: 1,
                    area: ['250px', '500px'],
                    fix: false, //不固定
                    maxmin: true,
                    shade: 0.4,
                    title: '菜单赋权',
                    content: $('#treeContent')
                });

            }
        });

    }

    /**
     * 根据ids删除
     * @param ids
     */
    function delGroup(ids) {
        if (ids.length < 1) {
            layer.msg("请选中数据");
            return;
        }
        layer.confirm('确认要删除吗？', function (index) {
            //此处请求后台程序，下方是成功后的前台处理……
            layer.close(index);
            $.ajax({
                url: basePath + '/page/admin/menu/del',
                type: 'post',
                data: {list: ids},
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        layer.msg(data.message);
                        search();
                    }
                }
            })

        });
    }

    /**
     * 添加
     */
    function addInfo() {
        layer_show("添加", basePath + '/page/admin/menu/add', '450', '550');
    }

    /**
     * 编辑
     * @param id
     */
    function editInfo(id) {
        layer_show("修改信息", basePath + '/page/admin/menu/edit?id=' + id, '450', '550');
    }

    /**
     * 搜索
     */
    function search() {
        if (dataTabel)
            dataTabel.ajax.reload(null, true);
    }

    return {
        init: function () {
            initDataTable();
        },
        delGroup: function () {
            delGroup(DataTableUtil.getChekIDs());
        },
        delItem: function (id) {
            delGroup([id]);
        },
        editinfo: editInfo,
        addinfo: addInfo,
        search: search,
        fq: fq
    };
}();