/**
 * Created by Administrator on 2017/6/9.
 */
var Role = function () {
    var dataTabel = undefined;
    var ztree = undefined;
    var curroleid = undefined;

    function initTree(data) {
        var setting = {
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onCheck: function (event, treeId, treeNode) {
                    changePermission(treeNode.id,treeNode.checked,treeNode.isParent);
                }
            }
        };
        ztree = $.fn.zTree.init($("#tree"), setting, data);
    }

    function initDataTable() {
        var tableUrl = basePath + "/page/admin/auth/role/list";
        var columns = [
            getColumn("id", getCheckBoxRender()),
            getColumn("rolename"),
            getColumn("rolesign"),
            getColumn("createTime"),
            getColumn("updateTime"),
            getColumn("description"),
            getColumn("", function (data, type, row, meta) {
                var style = 'text-decoration:none;margin-left: 4px;margin-right: 4px';
                var fq = "<a style='" + style + "' onClick='Role.addPermission(\"" + row.id + "\")' href='javascript:;' title='赋权'><i class='Hui-iconfont'>&#xe61d;</i></a>";
                var edit = "<a style='" + style + "' onClick='Role.editinfo(\"" + row.id + "\")' href='javascript:;' title='编辑'><i class='Hui-iconfont'>&#xe6df;</i></a>";
                var del = "<a style='" + style + "' class='ml-5' onClick='Role.delItem(\"" + row.id + "\")' href='javascript:;' title='删除'><i class='Hui-iconfont'>&#xe6e2;</i></a>";

                return fq + edit + del;
            })];
        var ajaxconfig = {
            url: tableUrl,
            type: 'POST',
            data: function (d) {
                d.keyword = $("#keyword").val();
            }
        };

        dataTabel = DataTableUtil.init('dtable', {
            columns: columns,
            ajaxconfig: ajaxconfig
        });
    }

    function changePermission(id, checked, isGroup) {
        var url = basePath + '/page/admin/auth/role/changepermission';

        $.ajax({
            url: url,
            type: 'post',
            data: {rid: curroleid, pid: id, checked: checked, isGroup: isGroup ? true : false},
            dataType: 'json',
            success: function (data) {
            }
        });
    }

    /**
     * 根据ids删除
     * @param ids
     */
    function delGroup(ids) {
        if (ids.length < 1) {
            layer.msg("请选中数据");
            return;
        }
        layer.confirm('确认要删除吗？', function (index) {
            //此处请求后台程序，下方是成功后的前台处理……
            layer.close(index);
            $.ajax({
                url: basePath + '/page/admin/auth/role/del',
                type: 'post',
                data: {list: ids},
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        layer.msg(data.message);
                        search();
                    }
                }
            })

        });
    }

    /**
     * 添加
     */
    function addInfo() {
        layer_show("添加", basePath + '/page/admin/auth/role/add', '450', '450');
    }

    /**
     * 编辑
     * @param id
     */
    function editInfo(id) {
        layer_show("修改信息", basePath + '/page/admin/auth/role/edit?id=' + id, '450', '450');
    }

    /**
     * 赋权
     * @param id
     */
    function addPermission(id) {
        curroleid = id;
        $.ajax({
            url: basePath + '/page/admin/auth/role/stree',
            type: 'post',
            data: {id: id},
            dataType: 'json',
            success: function (data) {
                initTree(data);
                layer.open({
                    type: 1,
                    area: ['250px', '500px'],
                    fix: false, //不固定
                    maxmin: true,
                    shade: 0.4,
                    title: '角色赋权',
                    content: $('#treeContent')
                });

            }
        });
    }

    /**
     * 搜索
     */
    function search() {
        if (dataTabel)
            dataTabel.ajax.reload(null, true);
    }

    return {
        init: function () {
            initDataTable();
        },
        delGroup: function () {
            delGroup(DataTableUtil.getChekIDs());
        },
        delItem: function (id) {
            delGroup([id]);
        },
        editinfo: editInfo,
        addinfo: addInfo,
        search: search,
        addPermission: addPermission
    };
}();