/**
 * Created by Administrator on 2017/6/12.
 */
var RoleEdit = function () {

    /**
     * 初始化表格校验
     */
    function initValite() {
        $("#form-add").validate({
            rules: {
                rolename: {
                    required: true
                },
                rolesign: {
                    required: true,
                }
            },
            onkeyup: false,
            focusCleanup: true,
            focusInvalid: false,
            // success: "valid",
            submitHandler: function (form) {
                $("#form-add").ajaxSubmit({
                    url: basePath + '/page/admin/auth/role/edit',
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            parent.search();
                            layer.msg(data.message, {icon: 1, time: 1000}, function () {
                                layer_close();
                            });
                        } else {
                            layer.msg(data.message, {icon: 1, time: 1000});
                        }
                    },
                    error: function (XmlHttpRequest, textStatus, errorThrown) {
                        console.log('error:' + textStatus);
                        console.info(errorThrown);
                    }
                });

                return false;
            }
        });
    }


    return {
        init: function () {
            initValite();
        }
    };
}();