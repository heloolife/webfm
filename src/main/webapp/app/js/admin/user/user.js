/**
 * Created by Administrator on 2017/6/9.
 */
var czid = undefined;
var czlayerindex = undefined;

var User = function () {
    var dataTabel = undefined;


    function initDataTable() {
        var tableUrl = basePath + "/page/admin/user/list";
        var columns = [
            getColumn("id", getCheckBoxRender()),
            getColumn("username"),
            getColumn("status", function (data, type, row, meta) {
                var status = '未激活';
                if (data == '1')
                    status = '激活';
                return status;
            }),
            getColumn("lastlogintime"),
            getColumn("lastloginip"),
            getColumn("createtime"),
            getColumn("updateTime"),
            getColumn("", function (data, type, row, meta) {
                var style = 'text-decoration:none;margin-left: 4px;margin-right: 4px';
                var fq = "<a style='" + style + "' onClick='User.addRole(\"" + row.id + "\")' href='javascript:;' title='赋角色'><i class='Hui-iconfont'>&#xe61d;</i></a>";
                var edit = "<a style='" + style + "' onClick='User.editinfo(\"" + row.id + "\")' href='javascript:;' title='修改密码'><i class='Hui-iconfont'>&#xe63f;</i></a>";
                var del = "<a style='" + style + "' class='ml-5' onClick='User.delItem(\"" + row.id + "\")' href='javascript:;' title='删除'><i class='Hui-iconfont'>&#xe6e2;</i></a>";

                return fq + edit + del;
            })];
        var ajaxconfig = {
            url: tableUrl,
            type: 'POST',
            data: function (d) {
                d.keyword = $("#keyword").val();
            }
        };

        dataTabel = DataTableUtil.init('dtable', {
            columns: columns,
            ajaxconfig: ajaxconfig
        });
    }

    function initTree() {
        ZtreeCb.init('tree');
        ZtreeCb.addCallBack({
            groupChecked: function (id, name, checked) {
                xgjs(id, checked, true);
            },
            childChecked: function (id, name, checked) {
                xgjs(id, checked, false);
            }
        });
    }

    /**
     * 修改角色
     * @param roleid
     * @param checked
     * @param isgroup
     */
    function xgjs(roleid, checked, isgroup) {
        $.ajax({
            url: basePath + '/page/admin/user/changepermission',
            type: 'post',
            data: {uid: czid, rid: roleid, checked: checked, isGroup: isgroup},
            dataType: 'json',
            success: function (data) {
            }
        })
    }

    /**
     * 根据ids删除
     * @param ids
     */
    function delGroup(ids) {
        if (ids.length < 1) {
            layer.msg("请选中数据");
            return;
        }
        layer.confirm('确认要删除吗？', function (index) {
            //此处请求后台程序，下方是成功后的前台处理……
            layer.close(index);
            $.ajax({
                url: basePath + '/page/admin/user/del',
                type: 'post',
                data: {list: ids},
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        layer.msg(data.message);
                        search();
                    }
                }
            })

        });
    }


    /**
     * 添加
     */
    function addInfo() {
        layer_show("添加", basePath + '/page/admin/user/add', '450', '330');
    }

    /**
     * 编辑
     * @param id
     */
    function editInfo(id) {
        czid = id;
        czlayerindex = layer.open({
            type: 1,
            area: ['300px', '200px'],
            fix: false, //不固定
            maxmin: true,
            shade: 0.4,
            title: '重置密码',
            content: $('#resetpwd')
        });
    }

    /**
     * 赋权
     * @param id
     */
    function addRole(id) {
        czid = id;
        $.ajax({
            url: basePath + '/page/admin/user/stree',
            type: 'post',
            data: {id: id},
            dataType: 'json',
            success: function (data) {
                ZtreeCb.load(data);
                layer.open({
                    type: 1,
                    area: ['250px', '500px'],
                    fix: false, //不固定
                    maxmin: true,
                    shade: 0.4,
                    title: '用户赋角色',
                    content: $('#treeContent')
                });
            }
        });
    }

    /**
     * 搜索
     */
    function search() {
        if (dataTabel)
            dataTabel.ajax.reload(null, true);
    }

    return {
        init: function () {
            initDataTable();
            initTree();
        },
        delGroup: function () {
            delGroup(DataTableUtil.getChekIDs());
        },
        delItem: function (id) {
            delGroup([id]);
        },
        editinfo: editInfo,
        addinfo: addInfo,
        search: search,
        addRole: addRole
    };
}();