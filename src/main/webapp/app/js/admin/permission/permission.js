/**
 * Created by Administrator on 2017/6/9.
 */
var Permission = function () {
    var dataTabel = undefined;


    function initDataTable() {
        var tableUrl = basePath + "/page/admin/auth/permissions";
        var columns = [
            getColumn("id", getCheckBoxRender()),
            getColumn("name"),
            getColumn("sign"),
            getColumn("type", function (data, type, row, meta) {
                var status = '分组';
                if (data == '1')
                    status = '权限';
                return status;
            }),
            getColumn("groupName"),
            getColumn("status", function (data, type, row, meta) {
                var status = '有效';
                if (data != '1')
                    status = '失效';
                return status;
            }),
            getColumn("createTime"),
            getColumn("updateTime"),
            getColumn("description"),
            getColumn("", function (data, type, row, meta) {
                var edit = "<a style='text-decoration:none' onClick='Permission.editinfo(\"" + row.id + "\")' href='javascript:;' title='编辑'><i class='Hui-iconfont'>&#xe6df;</i></a>";
                var del = "<a style='text-decoration:none' class='ml-5' onClick='Permission.delItem(\"" + row.id + "\")' href='javascript:;' title='删除'><i class='Hui-iconfont'>&#xe6e2;</i></a>";

                return edit + del;
            })];
        var ajaxconfig = {
            url: tableUrl,
            type: 'POST',
            data: function (d) {
                d.keyword = $("#keyword").val();
                d.groupid = $('#selectgroup').val();
            }
        };

        dataTabel = DataTableUtil.init('dtable', {
            columns: columns,
            ajaxconfig: ajaxconfig
        });
    }

    /**
     * 根据ids删除
     * @param ids
     */
    function delGroup(ids) {
        if (ids.length < 1) {
            layer.msg("请选中数据");
            return;
        }
        layer.confirm('确认要删除吗？\<br\>(注意！若该权限类型为分组，则删除该分组下的所有权限)', function (index) {
            //此处请求后台程序，下方是成功后的前台处理……
            layer.close(index);
            $.ajax({
                url: basePath + '/page/admin/auth/permission/del',
                type: 'post',
                data: {list: ids},
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        layer.msg(data.message);
                        search();
                    }
                }
            })

        });
    }

    /**
     * 添加
     */
    function addInfo() {
        layer_show("添加", basePath + '/page/admin/auth/permission/add', '480', '480');
    }

    /**
     * 编辑
     * @param id
     */
    function editInfo(id) {
        layer_show("修改信息", basePath + '/page/admin/auth/permission/edit?id=' + id, '480', '480');
    }

    /**
     * 搜索
     */
    function search() {
        if (dataTabel)
            dataTabel.ajax.reload(null, true);
    }

    return {
        init: function () {
            initDataTable();
        },
        delGroup: function () {
            delGroup(DataTableUtil.getChekIDs());
        },
        delItem: function (id) {
            delGroup([id]);
        },
        editinfo: editInfo,
        addinfo: addInfo,
        search: search
    };
}();