/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50715
Source Host           : localhost:3306
Source Database       : webfm

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2017-06-15 16:25:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_dictionary`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `pid` varchar(50) DEFAULT NULL COMMENT '父级id',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `showText` varchar(50) DEFAULT NULL COMMENT '显示文字',
  `code` varchar(50) DEFAULT NULL COMMENT '编码，唯一，用于关联',
  `value` varchar(200) DEFAULT NULL COMMENT '值，可与名称不同',
  `isDeleted` tinyint(1) DEFAULT '0' COMMENT '是否已删除',
  `isActive` tinyint(1) DEFAULT '1' COMMENT '1:启用/0:禁用',
  `description` varchar(512) DEFAULT NULL COMMENT '备注说明',
  `created` datetime NOT NULL COMMENT '创建时间',
  `createdBy` varchar(32) DEFAULT NULL COMMENT '创建人',
  `lastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `lastModifiedBy` varchar(32) DEFAULT NULL COMMENT '修改人',
  `reserve1` varchar(200) DEFAULT NULL,
  `reserve2` varchar(200) DEFAULT NULL,
  `reserve3` varchar(200) DEFAULT NULL,
  `reserve4` varchar(200) DEFAULT NULL,
  `customCode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' 字典管理';

-- ----------------------------
-- Records of sys_dictionary
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(32) NOT NULL,
  `userId` varchar(50) NOT NULL COMMENT '后台操作用户',
  `memberId` varchar(50) NOT NULL COMMENT '前台操作用户',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip',
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  `action` varchar(20) NOT NULL COMMENT '操作',
  `actionType` varchar(20) NOT NULL COMMENT '操作类型 front-前台 manager-后台',
  `actionContent` varchar(500) NOT NULL COMMENT '操作内容',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `customCode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sys_log_sys_user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统操作日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `pid` varchar(32) DEFAULT NULL COMMENT '父节点id',
  `name` varchar(32) DEFAULT NULL,
  `icon` varchar(20) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `sort` int(20) DEFAULT NULL,
  `status` int(3) NOT NULL DEFAULT '1',
  `description` varchar(256) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('6732174e7c9c4da783d8513c37d7fdf1', '', '系统设置', '&#xe61d;', '', '20', '1', '系统基础设置菜单集合', '2017-06-15 14:47:06', '2017-06-15 14:47:06');
INSERT INTO `sys_menu` VALUES ('71ee41185f404aa1b8193e80a8200481', '6732174e7c9c4da783d8513c37d7fdf1', '角色管理', '', '/page/admin/auth/role', '2', '1', '左侧角色管理', '2017-06-15 15:23:09', '2017-06-15 15:23:09');
INSERT INTO `sys_menu` VALUES ('a180f677e93c4893a6abb46cc7127b00', '6732174e7c9c4da783d8513c37d7fdf1', '权限管理', '', '/page/admin/auth/permission', '3', '1', '左侧权限管理', '2017-06-15 15:16:10', '2017-06-15 15:16:10');
INSERT INTO `sys_menu` VALUES ('b2e102a745a349de97f150d143eda036', '6732174e7c9c4da783d8513c37d7fdf1', '菜单管理', '', '/page/admin/menu/list', '4', '1', '左侧菜单栏管理', '2017-06-15 15:13:02', '2017-06-15 15:13:02');
INSERT INTO `sys_menu` VALUES ('e3a7b94bd60c4b3ba9460da79e2d36bf', '6732174e7c9c4da783d8513c37d7fdf1', '用户管理', '', '/page/admin/user/list', '1', '1', '左侧用户管理', '2017-06-15 15:13:48', '2017-06-15 15:13:48');

-- ----------------------------
-- Table structure for `sys_menu_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_permission`;
CREATE TABLE `sys_menu_permission` (
  `menu_id` varchar(32) NOT NULL,
  `permission_id` varchar(32) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_permission
-- ----------------------------
INSERT INTO `sys_menu_permission` VALUES ('32d8b94c5de84c4b98ca9b5679a25372', 'bb4d1754b2d543feaac7227934c1591c', '2017-06-15 09:26:35');
INSERT INTO `sys_menu_permission` VALUES ('6732174e7c9c4da783d8513c37d7fdf1', 'c262f045e77748ff9b8e9cd4b5d67511', '2017-06-15 15:49:03');
INSERT INTO `sys_menu_permission` VALUES ('71ee41185f404aa1b8193e80a8200481', '61ebf9080cea4d3897aebb94207ea664', '2017-06-15 16:08:10');
INSERT INTO `sys_menu_permission` VALUES ('a180f677e93c4893a6abb46cc7127b00', '2b98c42519684d98bfc94e1b10228734', '2017-06-15 16:10:03');
INSERT INTO `sys_menu_permission` VALUES ('b2e102a745a349de97f150d143eda036', '8b3093665eb04f9bb2857256bbdba03b', '2017-06-15 16:10:22');
INSERT INTO `sys_menu_permission` VALUES ('e3a7b94bd60c4b3ba9460da79e2d36bf', 'a294165d92b144e4b3909d860ed153eb', '2017-06-15 16:10:28');

-- ----------------------------
-- Table structure for `sys_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` varchar(32) NOT NULL COMMENT '权限id',
  `pid` varchar(32) DEFAULT NULL COMMENT '权限分组',
  `name` varchar(32) DEFAULT NULL COMMENT '权限名',
  `sign` varchar(128) DEFAULT NULL COMMENT '权限标识,程序中判断使用,如"user:create"',
  `status` char(1) DEFAULT '1' COMMENT '0-无效 1-有效',
  `type` int(2) NOT NULL DEFAULT '1' COMMENT '权限类型  1为权限 2为权限分组',
  `description` varchar(256) DEFAULT NULL COMMENT '权限描述,url为空则为权限分组',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_sign` (`sign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 ROW_FORMAT=DYNAMIC COMMENT='权限表-对应功能管理';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('2b98c42519684d98bfc94e1b10228734', 'b1c5b4b6effb4d659c6836e847e608ba', '权限管理', 'sign_menu_system_permission', '1', '1', '系统设置 二级菜单 权限', '2017-06-15 15:44:09', '2017-06-15 15:44:09');
INSERT INTO `sys_permission` VALUES ('61ebf9080cea4d3897aebb94207ea664', 'b1c5b4b6effb4d659c6836e847e608ba', '角色管理', 'sign_menu_system_role', '1', '1', '系统设置 二级菜单 角色权限', '2017-06-15 15:42:19', '2017-06-15 15:42:19');
INSERT INTO `sys_permission` VALUES ('8b3093665eb04f9bb2857256bbdba03b', 'b1c5b4b6effb4d659c6836e847e608ba', '菜单管理', 'sign_menu_system_menu', '1', '1', '系统设置 二级菜单 菜单管理', '2017-06-15 15:44:51', '2017-06-15 15:44:51');
INSERT INTO `sys_permission` VALUES ('a294165d92b144e4b3909d860ed153eb', 'b1c5b4b6effb4d659c6836e847e608ba', '用户管理', 'sign_menu_system_user', '1', '1', '系统设置 二级菜单 用户权限', '2017-06-15 15:40:33', '2017-06-15 15:40:33');
INSERT INTO `sys_permission` VALUES ('b1c5b4b6effb4d659c6836e847e608ba', '', '菜单分组', 'sign_menu', '1', '2', '菜单所需权限分组', '2017-06-15 15:38:25', '2017-06-15 15:38:25');
INSERT INTO `sys_permission` VALUES ('c262f045e77748ff9b8e9cd4b5d67511', 'b1c5b4b6effb4d659c6836e847e608ba', '系统设置', 'sign_menu_system', '1', '1', '系统设置一级菜单权限', '2017-06-15 15:39:05', '2017-06-15 15:39:05');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(32) NOT NULL COMMENT '角色id',
  `roleName` varchar(32) DEFAULT NULL COMMENT '角色名',
  `roleSign` varchar(128) DEFAULT NULL COMMENT '角色标识,程序中判断使用,如"admin"',
  `description` varchar(256) DEFAULT NULL COMMENT '角色描述,UI界面显示使用',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_sign` (`roleSign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('de788f5f67d84dcca288f2e05440d23f', '超级管理员', 'super', '拥有所有权限的超级管理员', '2017-06-15 15:47:47', '2017-06-15 15:47:47');

-- ----------------------------
-- Table structure for `sys_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `role_id` varchar(32) NOT NULL COMMENT '角色id',
  `permission_id` varchar(32) NOT NULL COMMENT '权限id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `FK_sys_role_permission_sys_role` (`role_id`),
  KEY `FK_sys_role_permission_sys_permission` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 ROW_FORMAT=DYNAMIC COMMENT='角色与权限关联表';

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES ('36bceeaa975347d0ab3c656885f4ee84', '069ae8a5bab247f2a566436968864460', '2017-06-14 16:11:30');
INSERT INTO `sys_role_permission` VALUES ('36bceeaa975347d0ab3c656885f4ee84', 'bb4d1754b2d543feaac7227934c1591c', '2017-06-14 16:11:30');
INSERT INTO `sys_role_permission` VALUES ('36bceeaa975347d0ab3c656885f4ee84', 'c480183c3f42427db34b2d0354b59397', '2017-06-14 16:11:30');
INSERT INTO `sys_role_permission` VALUES ('36bceeaa975347d0ab3c656885f4ee84', 'c6a3d026f2364561a0d910e43ba7cfec', '2017-06-15 09:29:42');
INSERT INTO `sys_role_permission` VALUES ('de788f5f67d84dcca288f2e05440d23f', '2b98c42519684d98bfc94e1b10228734', '2017-06-15 15:47:57');
INSERT INTO `sys_role_permission` VALUES ('de788f5f67d84dcca288f2e05440d23f', '61ebf9080cea4d3897aebb94207ea664', '2017-06-15 15:47:57');
INSERT INTO `sys_role_permission` VALUES ('de788f5f67d84dcca288f2e05440d23f', '8b3093665eb04f9bb2857256bbdba03b', '2017-06-15 15:47:57');
INSERT INTO `sys_role_permission` VALUES ('de788f5f67d84dcca288f2e05440d23f', 'a294165d92b144e4b3909d860ed153eb', '2017-06-15 15:47:57');
INSERT INTO `sys_role_permission` VALUES ('de788f5f67d84dcca288f2e05440d23f', 'c262f045e77748ff9b8e9cd4b5d67511', '2017-06-15 15:47:57');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(32) NOT NULL COMMENT '用户id',
  `userName` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `salt` varchar(50) DEFAULT NULL COMMENT 'salt值',
  `status` int(2) DEFAULT '1' COMMENT '状态 默认1 启用',
  `type` int(2) NOT NULL DEFAULT '0' COMMENT '0为普通用户 ',
  `lastLoginTime` datetime DEFAULT NULL COMMENT '最后登录时间',
  `lastLoginIp` varchar(16) DEFAULT NULL COMMENT '最后登录ip',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 ROW_FORMAT=DYNAMIC COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('3cfe04cb1a9542c79df79669c3a1def6', 'admin', 'd9ca9e251bdeaf9f142f5f24b2a07eee', '1ed071d55c3c791bc438f2c68ed8bde0', '1', '0', null, null, '2017-06-15 16:11:10', '2017-06-15 16:11:10');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `role_id` varchar(32) NOT NULL COMMENT '角色id',
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `FK_sys_user_role_sys_role` (`role_id`),
  KEY `FK_sys_user_role_sys_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 ROW_FORMAT=DYNAMIC COMMENT='角色与用户关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('de788f5f67d84dcca288f2e05440d23f', '3cfe04cb1a9542c79df79669c3a1def6', '2017-06-15 16:11:20');
