#webfm
init sql:
CREATE TABLE `sys_log` (
	`id` VARCHAR(32) NOT NULL,
	`userId` VARCHAR(50) NOT NULL COMMENT '后台操作用户',
	`memberId` VARCHAR(50) NOT NULL COMMENT '前台操作用户',
	`createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
	`ip` VARCHAR(20) NULL DEFAULT NULL COMMENT 'ip',
	`url` VARCHAR(255) NULL DEFAULT NULL COMMENT '路径',
	`action` VARCHAR(20) NOT NULL COMMENT '操作',
	`actionType` VARCHAR(20) NOT NULL COMMENT '操作类型 front-前台 manager-后台',
	`actionContent` VARCHAR(500) NOT NULL COMMENT '操作内容',
	`remark` VARCHAR(200) NULL DEFAULT NULL COMMENT '备注',
	`customCode` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_sys_log_sys_user` (`userId`)
)
COMMENT='系统操作日志表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `sys_permission` (
	`id` VARCHAR(32) NOT NULL COMMENT '权限id',
	`pid` VARCHAR(32) NULL DEFAULT '' COMMENT '父id',
	`permission_name` VARCHAR(32) NULL DEFAULT NULL COMMENT '权限名',
	`permission_sign` VARCHAR(128) NULL DEFAULT NULL COMMENT '权限标识,程序中判断使用,如"user:create"',
	`permission_type` VARCHAR(20) NULL DEFAULT 'function' COMMENT '权限类型 menu-菜单 function-功能',
	`is_show_menu` CHAR(1) NULL DEFAULT '1' COMMENT '该功能是否出现在菜单上0-不显示 1-显示',
	`menu_icon` VARCHAR(20) NULL DEFAULT '1' COMMENT '菜单图标',
	`url` VARCHAR(100) NULL DEFAULT '' COMMENT '内部链接地址。类型为功能可空',
	`status` CHAR(1) NULL DEFAULT '1' COMMENT '0-无效 1-有效',
	`sort_no` INT(11) NULL DEFAULT '1' COMMENT '排序号',
	`description` VARCHAR(256) NULL DEFAULT '' COMMENT '权限描述,UI界面显示使用',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `permission_sign` (`permission_sign`)
)
COMMENT='权限表-对应功能管理'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
CHECKSUM=1
;

CREATE TABLE `sys_role` (
	`id` VARCHAR(32) NOT NULL COMMENT '角色id',
	`roleName` VARCHAR(32) NULL DEFAULT NULL COMMENT '角色名',
	`roleSign` VARCHAR(128) NULL DEFAULT NULL COMMENT '角色标识,程序中判断使用,如"admin"',
	`description` VARCHAR(256) NULL DEFAULT NULL COMMENT '角色描述,UI界面显示使用',
	`customCode` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `role_sign` (`roleSign`)
)
COMMENT='角色表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
CHECKSUM=1
;


CREATE TABLE `sys_role_permission` (
	`id` VARCHAR(32) NOT NULL COMMENT '表id',
	`role_id` VARCHAR(32) NULL DEFAULT NULL COMMENT '角色id',
	`permission_id` VARCHAR(32) NULL DEFAULT NULL COMMENT '权限id',
	`customCode` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_sys_role_permission_sys_role` (`role_id`),
	INDEX `FK_sys_role_permission_sys_permission` (`permission_id`),
	CONSTRAINT `FK_sys_role_permission_sys_permission` FOREIGN KEY (`permission_id`) REFERENCES `sys_permission` (`id`),
	CONSTRAINT `FK_sys_role_permission_sys_role` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`)
)
COMMENT='角色与权限关联表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
CHECKSUM=1
;


CREATE TABLE `sys_dictionary` (
	`id` VARCHAR(32) NOT NULL COMMENT '主键',
	`pid` VARCHAR(50) NULL DEFAULT NULL COMMENT '父级id',
	`name` VARCHAR(50) NULL DEFAULT NULL COMMENT '名称',
	`showText` VARCHAR(50) NULL DEFAULT NULL COMMENT '显示文字',
	`code` VARCHAR(50) NULL DEFAULT NULL COMMENT '编码，唯一，用于关联',
	`value` VARCHAR(200) NULL DEFAULT NULL COMMENT '值，可与名称不同',
	`isDeleted` TINYINT(1) NULL DEFAULT '0' COMMENT '是否已删除',
	`isActive` TINYINT(1) NULL DEFAULT '1' COMMENT '1:启用/0:禁用',
	`description` VARCHAR(512) NULL DEFAULT NULL COMMENT '备注说明',
	`created` DATETIME NOT NULL COMMENT '创建时间',
	`createdBy` VARCHAR(32) NULL DEFAULT NULL COMMENT '创建人',
	`lastModified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
	`lastModifiedBy` VARCHAR(32) NULL DEFAULT NULL COMMENT '修改人',
	`reserve1` VARCHAR(200) NULL DEFAULT NULL,
	`reserve2` VARCHAR(200) NULL DEFAULT NULL,
	`reserve3` VARCHAR(200) NULL DEFAULT NULL,
	`reserve4` VARCHAR(200) NULL DEFAULT NULL,
	`customCode` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `code` (`code`)
)
COMMENT=' 字典管理'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `sys_user` (
	`id` VARCHAR(32) NOT NULL COMMENT '用户id',
	`userName` VARCHAR(50) NULL DEFAULT NULL COMMENT '用户名',
	`password` VARCHAR(128) NULL DEFAULT NULL COMMENT '密码',
  `salt` VARCHAR(50) NULL DEFAULT NULL COMMENT 'salt值',
	`createTime` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
	`lastLoginTime` DATETIME NULL DEFAULT NULL COMMENT '最后登录时间',
	`lastLoginIp` VARCHAR(16) NULL DEFAULT NULL COMMENT '最后登录ip',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `username` (`userName`)
)
COMMENT='系统用户表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
CHECKSUM=1
;

CREATE TABLE `sys_user_role` (
	`id` VARCHAR(32) NOT NULL COMMENT '表id',
	`role_id` VARCHAR(32) NULL DEFAULT NULL COMMENT '角色id',
	`user_id` VARCHAR(32) NULL DEFAULT NULL COMMENT '用户id',
	PRIMARY KEY (`id`),
	INDEX `FK_sys_user_role_sys_role` (`role_id`),
	INDEX `FK_sys_user_role_sys_user` (`user_id`),
	CONSTRAINT `FK_sys_user_role_sys_user` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`),
	CONSTRAINT `FK_sys_user_role_sys_role` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`)
)
COMMENT='角色与用户关联表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
CHECKSUM=1
;